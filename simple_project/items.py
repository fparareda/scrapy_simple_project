# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class WikipediaItem(scrapy.Item):
    title = scrapy.Field()
    excerpt_content = scrapy.Field()
    relative_img_url = scrapy.Field()
