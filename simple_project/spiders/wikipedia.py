import scrapy

from simple_project.items import WikipediaItem


class WikipediaSpider(scrapy.Spider):
    name = 'wikipedia'
    allowed_domains = ['wikipedia.org']

    def __init__(self, *args, **kwargs):
        self.start_urls = [
            "https://en.wikipedia.org/wiki/COVID-19_pandemic",
        ]
        super().__init__(*args, **kwargs)

    def parse(self, response):

        # Getting content from the HTML
        title = response.xpath("//h1[@id='firstHeading']/text()").extract_first()
        excerpt_content = response.xpath("//div[@class='excerpt']/text()").extract()
        relative_img_url = response.xpath("//table[@class='infobox']//img//@src").extract_first()

        # Create the item to manage it properly
        yield WikipediaItem(
            title=title,
            excerpt_content=''.join(excerpt_content), # unifying all the strings coming from the dirty html format
            relative_img_url=relative_img_url)
