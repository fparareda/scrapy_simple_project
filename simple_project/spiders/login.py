import scrapy
from scrapy.http import Request, FormRequest
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor


class LoginSpider(scrapy.Spider):

    name = 'login_spider'
    allowed_domains = ['example.com']
    login_page = 'http://www.example.com/login'
    start_urls = ['http://www.example.com/useful_page/']

    # You can create rules to scrape
    rules = (
        # Rules specifically when the URLs contains specific text
        Rule(
            LinkExtractor(allow='/product.php?id=\d+'),
            callback='parse_item'
        ),
    )

    def init_request(self):
        return Request(url=self.login_page, callback=self.login)

    def login(self, response):
        # Let's going to simulate a manual login
        return FormRequest.from_response(response,
                                         formdata={'name': 'username', 'password': 'password'},
                                         callback=self.check_login_response)

    def check_login_response(self, response):
        # Check the response returned by a login request to see if we are successfully logged in
        if "Hi username" in response.body:
            # Great! We are in! Let's start to scrape!
            return self.initialized()
        else:
            # Oops! Something went wrong, something wrong happened.
            self.log("Not possible to login")


    def parse(self, response, **kwargs):
        # Retrieve data from the website generically
        pass

    def parse_item(self, response):
        # Retrieve data from the website using regular expresion in the url
        pass
